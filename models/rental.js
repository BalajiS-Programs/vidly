const Joi = require('joi');
const moment = require('moment');
const mongoose = require('mongoose');

const {Customer} = require('./customer');
const {Movie} = require('./movie');

const rentalSchema = new mongoose.Schema({
    customer: new mongoose.Schema({
        name: Customer.schema.obj.name,
        isGold: Customer.schema.obj.isGold,
        phone: Customer.schema.obj.phone
    }),
    movie: new mongoose.Schema({
        title: Movie.schema.obj.title,
        dailyRentalRate: Movie.schema.obj.dailyRentalRate
    }),
    dateOut: {
        type: Date,
        default: Date.now
    },
    dateReturned: {
        type: Date
    },
    rentalFee: {
        type: Number,
        min: 0
    }
});
rentalSchema.statics.lookup = function (customerId, movieId) {
    return this.findOne({
        'customer._id': customerId,
        'movie._id': movieId
    });
}

rentalSchema.methods.return = function (){
    this.dateReturned = new Date();

    const rentalDays = moment().diff(this.dateOut, 'days');
    this.rentalFee = rentalDays * this.movie.dailyRentalRate;
}

const Rental = mongoose.model('rental', rentalSchema);

function validateRental(rental) {
    const schema = {
        customerId: Joi.objectId().required(),
        movieId: Joi.objectId().required()
    };        
    console.log('*');
    
    return Joi.validate(rental, schema);
}

exports.Rental = Rental;
exports.validateRental = validateRental;
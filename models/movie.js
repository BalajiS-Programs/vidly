const Joi = require('joi');
const mongoose = require('mongoose');

const { Genre } = require('./genre');

const Movie = mongoose.model('Movie', new mongoose.Schema({
    title: {
        type: String, 
        trim: true, 
        minlength: 3, 
        maxlength: 30, 
        required: true
    }, 
    genre: {
        type: Genre.schema,
        required: true
    }, 
    numberInStock: {
        type: Number, 
        required: true
    }, 
    dailyRentalRate: {
        type: Number, 
        required: true
    }
}));

function validateMovie(movie) {
    const schema = {
        title: Joi.string().min(3).max(30).required(), 
        genreId: Joi.objectId().required(), // Client can enter only genreId
        numberInStock: Joi.number().min(0).max(100).required(), 
        dailyRentalRate: Joi.number().min(0).max(100).required()
    };

    return Joi.validate(movie, schema);
}

exports.Movie = Movie;
exports.validateMovie = validateMovie;
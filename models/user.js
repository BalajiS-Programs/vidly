const config = require('config');
const jwt = require('jsonwebtoken');

const Joi = require('joi');
const mongoose = require('mongoose');
const PasswordComplexity = require('joi-password-complexity');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 30
    },
    email: {
        type: String,
        required: true,
        unique: true,
        minlength: 5,
        maxlength: 50
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 1024
    }, 
    isAdmin: Boolean
});
userSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({
        _id: this._id, 
        isAdmin: this.isAdmin
    }, config.get('jwtPrivateKey'));
    return token;
};

const User = mongoose.model('user', userSchema);

const complexityOptions = {
    min: 5,
    max: 255,
    lowerCase: 1,
    upperCase: 1,
    numeric: 1,
    symbol: 1
};

function validateUser(user) {
    const schema = {
        name: Joi.string().min(3).max(30).required(), 
        email: Joi.string().min(5).max(100).email().required(), 
        password: Joi.string().min(5).max(255).required(), 
        isAdmin: Joi.boolean().required()
    };

    let password_validation_error = {};
    Joi.validate(user.password, new PasswordComplexity(complexityOptions), (err, password) => {
            password_validation_error.error = err;
            password_validation_error.password = password;
    });      
    if(password_validation_error.error)
        return password_validation_error;

    return Joi.validate(user, schema);     
}

exports.User = User;
exports.validateUser = validateUser;
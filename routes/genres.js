const express = require('express');
const mongoose = require('mongoose');

const validateObjectId = require('../middlewares/validateObjectId');
const auth = require('../middlewares/auth');
const validate = require('../middlewares/validate');
const admin = require('../middlewares/admin');
// const asyncMiddleware = require('../middlewares/async');

const {Genre, validateGenre} = require('../models/genre');

const router = express.Router();

// router.get('/', auth, asyncMiddleware(async (req, res) => {
//   const genres = await Genre.find().sort('name');
//   res.send(genres);
// }));

router.get('/', async (req, res) => { 
    
    const genres = await Genre.find().sort('name');
    res.send(genres);  
});

router.post('/', [auth, validate(validateGenre)], async(req, res) => {
 
  var genre = new Genre({
    name: req.body.name
  });
// console.log(genre._id);

    await genre.save();
    res.send(genre);
});

router.put('/:id', [auth, validate(validateGenre)], async(req, res) => {
  
  const genre = await Genre.findByIdAndUpdate(req.params.id, {
    name: req.body.name
  }, {
    new: true
  });  
  res.send(genre);
});

router.delete('/:id', [auth, admin], async (req, res) => {
  const genre = await Genre.findByIdAndRemove(req.params.id);
  if (!genre) return res.status(404).send('The genre with the given ID was not found.');

  res.send(genre);
});

router.get('/:id', validateObjectId, async(req, res) => {  
  const genre = await Genre.findById(req.params.id);
  if (!genre) return res.status(404).send('The genre with the given ID was not found.');
  res.send(genre);
});



module.exports = router;
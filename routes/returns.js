const express = require('express');
const Joi = require('joi');

const {Rental} = require('../models/rental');
const {Movie} = require('../models/movie');

const auth = require('../middlewares/auth');
const validate = require('../middlewares/validate');

const router = express.Router();

router.post('/', [auth, validate(validateReturn)], async (req, res) => {

    const rental = await Rental.lookup(req.body.customerId, req.body.movieId);
    if(!rental) return res.status(404).send('Rental for the given customer and movie is not found');
    if (rental.dateReturned) return res.status(400).send('Return already processed');

    rental.return();
    await rental.save();

    await Movie.update({_id: rental.movie._id}, {
        $inc: {
            numberInStock: 1
        }
    });
    return res.status(200).send(rental);

    // res.status(401).send('Unauthorized');
});

function validateReturn(req) {
    const schema = {
        customerId: Joi.objectId().required(), 
        movieId: Joi.objectId().required()
    };

    return Joi.validate(req, schema);
}

module.exports = router;
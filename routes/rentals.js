const {
    Rental, 
    validateRental
} = require('../models/rental');
const {
    Movie
} = require('../models/movie');
const {
    Customer
} = require('../models/customer');

const auth = require('../middlewares/auth');
const validate = require('../middlewares/validate');

const mongoose = require('mongoose');
const express = require('express');
const Fawn = require('fawn');

const router = express.Router();
Fawn.init(mongoose);

router.get('/', auth, async(req, res) => {
    const rentals = await Rental.find().sort('-dateOut');
    res.send(rentals);
});

router.get('/:id', auth, async(req, res) => {
    const rental = await Rental.findById(req.params.id);
    if(!rental) return res.status(400).send('Invalid Rental Id!');
    res.send(rental);
});

router.post('/', [auth, validate(validateRental)], async(req, res) => {
     
    const customer = await Customer.findById(req.body.customerId);
    if(!customer) return res.status(400).send('Invalid Customer Id!');

    const movie = await Movie.findById(req.body.movieId);
    if (!movie) return res.status(400).send('Invalid Movie Id!');

    if(movie.numberInStock === 0) return res.status(400).send('Requested Movie is Out of Stock');

    var rental = new Rental({
        customer: {
            _id: customer._id, 
            name: customer.name, 
            phone: customer.phone
        }, 
        movie: {
            _id: movie._id, 
            title: movie.title, 
            dailyRentalRate: movie.dailyRentalRate
        }
    });

    // Create a Task obj(it is like a transaction)
    // Here we can add one or more operation.  All are treated as a single operation

    try {
        new Fawn.Task()
            .save('rentals', rental)
            .update('movies', {
                _id: movie._id
            }, {
                $inc: {
                    numberInStock: -1
                }
            })
            .run();
        return res.send(rental);
    } catch (err) {
        return res.status(500).send('Something Failed.');
        // 500 - Internal Server Error
    }
    

        // .remove(); // We also want to update movie obj
        // 2nd arg query obj that determines the movie to be updated
        // 3rd arg is a update operation
        //Here we are directly working with the collection.  So, we use the actual plural name.  Collection name is case sensitive.  2nd arg is the actual rental obj

    //Now check your database.  You will see a new collection gets created.  It is created by Fawn to perform 2 phase commit.  When you perform run(), it will insert a new document into that collection which represents that transaction.  Then it performs all operations insist by us.  When all executes, then it will remove that document from that collection.

    // MongoDB won't set default value for our field.  Mongoose will set the default value for us before we actually save it and it also set the object id.  Object Id also set by our mongoose not by the mongodb

});

router.put('/:id', auth, async(req, res) => {

    const customer = await Customer.findById(req.body.customerId);
    if (!customer) return res.status(400).send('Invalid Customer Id!');

    const movie = await Movie.findById(req.body.movieId);
    if (!movie) return res.status(400).send('Invalid Movie Id!');

    var rental = await Rental.findByIdAndUpdate(req.params.id, {
            customer: {
                _id: customer._id,
                name: customer.name,
                phone: customer.phone
            },
            movie: {
                _id: movie._id,
                title: movie.title,
                dailyRentalRate: movie.dailyRentalRate
            }
    }, {
        new: true
    });
    if (!rental) return res.status(400).send('Invalid Rental Id!');
    
    return res.send(rental);
});

router.delete('/:id', auth, async(req, res) => {
    const rental = await Rental.findByIdAndRemove(req.params.id);
    if (!rental) return res.status(400).send('Invalid Rental Id');
    return res.send(rental);
});

module.exports = router;
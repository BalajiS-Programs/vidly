const express = require('express');
const mongoose = require('mongoose');

const auth = require('../middlewares/auth');
const validate = require('../middlewares/validate');

const {Customer, validateCustomer} = require('../models/customer');

const router = express.Router();

router.post('/', validate, async(req, res) => {
    
    var customer = new Customer({
        name: req.body.name, 
        phone: req.body.phone, 
        isGold: req.body.isGold
    });
    
    await customer.save();
    return res.send(customer);
});

router.get('/', auth, async(req, res) => {
    const customers = await Customer.find().sort('name');
    return res.send(customers);
});

router.get('/:id', auth, async(req, res) => {
    const customer = await Customer.findById(req.params.id).sort('name');
    if (!customer) return res.status(400).send('Invalid Customer Id');
    return res.send(customer);
});

router.delete('/:id', auth, async(req, res) => {
    const customer = await Customer.findByIdAndRemove(req.params.id);
    if (!customer) return res.status(400).send('Invalid Customer Id');
    return res.send(customer);
});

router.put('/:id', [auth, validate], async(req, res) => {
    
    const customer = await Customer.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        phone: req.body.phone, 
        isGold: req.body.isGold
    }, {
        new: true
    });
    if (!customer) return res.status(400).send('Invalid Customer Id');
    return res.send(customer);
});

module.exports = router;
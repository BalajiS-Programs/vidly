const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

const {
    User,
    validateUser
} = require('../models/user');

const auth = require('../middlewares/auth');
const validate = require('../middlewares/validate');

const router = express.Router();

router.get('/me', auth, async (req, res) => {
    const user = await User
                    .findById(req.user._id)
                    .select('-password');
    return res.send(user);
});

router.post('/', validate(validateUser), async (req, res) => {    

    let user = await User.findOne({email: req.body.email});
    // console.log(user);
    
    if(user) return res.status(400).send('Existing User cant able to register again!');

    user = new User(_.pick(req.body, ['name', 'email', 'password', 'isAdmin']));

    const salt = await bcrypt.genSalt(13);
    user.password = await bcrypt.hash(user.password, salt);

    await user.save();
    const token = user.generateAuthToken();

    return res.header('x-auth', token).send(_.pick(user, ['_id', 'name', 'email', 'isAdmin']));
});

module.exports = router;
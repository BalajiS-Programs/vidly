const express = require('express');
const mongoose = require('mongoose');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const _ = require('lodash');

const { User } = require('../models/user');
const validate = require('../middlewares/validate');

const router = express.Router();

router.post('/', validate(validateAuth), async (req, res) => {
    const { error } = validateAuth(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({
        email: req.body.email
    });
    if (!user) return res.status(400).send('Invalid email id or password!');
    
    const valid = await bcrypt.compare(req.body.password, user.password);
    if (!valid) return res.status(400).send('Invalid email id or password!');

    const token = user.generateAuthToken();
    return res.send(token);
});

function validateAuth(req) {
    const schema = {       
        email: Joi.string().min(5).max(100).email().required(),
        password: Joi.string().min(5).max(255).required()
    };
    return Joi.validate(req, schema);
}

module.exports = router;
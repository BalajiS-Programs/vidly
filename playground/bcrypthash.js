const bcrypt = require('bcrypt');

async function run(password) {
    const salt = await bcrypt.genSalt(13);
    const hashedPassword = await bcrypt.hash(password, salt);
    console.log(hashedPassword);
}
run('123');


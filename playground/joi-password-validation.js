const Joi = require('joi');
const PasswordComplexity = require('joi-password-complexity');

const complexityOptions = {
    min: 1,
    max: 30,
    lowerCase: 1,
    upperCase: 1,
    numeric: 1,
    symbol: 1
}

Joi.validate('aPa2@s', new PasswordComplexity(complexityOptions), (err, value) => {
    console.log(value);
    console.log('******************************************');
    
    console.log(err)
});
// let err = 'dfa';
// Joi.validate('adf', new PasswordComplexity(complexityOptions), err);
// console.log(err);

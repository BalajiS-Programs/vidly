const moment = require('moment');

let dt = moment();
console.log(dt);
console.log(dt.toDate());

const sevenDaysBefore = moment().add(-7, 'days').toDate();
console.log(sevenDaysBefore);

const diff = moment().diff(sevenDaysBefore, 'days');
console.log(diff);
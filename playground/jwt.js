const jwt = require('jsonwebtoken');

const obj = {
    id: 123, 
    name: 'Sai', 
    email: 'sai@gmail.com'
};

// const token = jwt.sign('obj', 'MySecret');
const token = jwt.sign(obj, 'MySecret');
console.log(token);

// sayHello();
// const sayHello = () => console.log('Hello');
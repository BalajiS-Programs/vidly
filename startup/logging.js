const winston = require('winston');
// require('winston-mongodb');
require('express-async-errors'); // This module is also related to logging

module.exports = function() {
    winston.add(winston.transports.File, {
        filename: 'logfile.log'
    });
    // winston.add(winston.transports.MongoDB, {
    //     db: 'mongodb://localhost/vidly',
    //     level: 'info'
    // });
    // winston.remove(winston.transports.Console);

    winston.handleExceptions(
        new winston.transports.Console({
            colorize: true, 
            prettyPrint: true
        }), 
        new winston.transports.File({
            filename: 'uncaughtExceptions.log'
        })
    );

    process.on('unhandledRejection', (ex) => {
        throw ex;
    });

    // process.on('uncaughtException', (ex) => {
    //     console.log(ex.message);    
    //     winston.error(ex.message, ex);    
    //     // process.exit(1);
    // });
};
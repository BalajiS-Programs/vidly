const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    const token = req.header('x-auth');
    if(!token) return res.status(401).send('x-auth header not found!');

    try {
        const payload = jwt.verify(token, config.get('jwtPrivateKey'));
        req.user = payload;
        next();
    } catch (err) {
        return res.status(400).send('Invalid x-auth token!');
    }
}
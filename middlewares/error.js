const winston = require('winston');

module.exports = function (err, req, res, next) {
    // console.log(err.message);   
    winston.error(err.message, err);       
    return res.status(500).send('Internal Server Error!');
};
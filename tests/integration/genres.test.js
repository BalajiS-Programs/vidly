const mongoose = require('mongoose');
const request = require('supertest');

const {
    Genre
} = require('../../models/genre');
const {
    User
} = require('../../models/user');

describe('/api/genres', () => {
let server;
  beforeEach(() => {
      server = require('../../index');
  })
  afterEach(async () => {
      await server.close();
      await Genre.remove({});      
  });

    describe('GET /', () => {
        it('should get all the available Genres', async () => {
            await Genre.collection.insertMany([{
                    name: 'genre1'
                },
                {
                    name: 'genre2'
                }
            ]);

            const response = await request(server).get('/api/genres');
            expect(response.status).toBe(200);
            expect(response.body.length).toBe(2);
            expect(response.body.some(g => g.name === 'genre1')).toBeTruthy();
            expect(response.body.some(g => g.name === 'genre2')).toBeTruthy();
        });
    });

    describe('GET /:id', async () => {
        it('should return the genre, if the id is valid', async () => {
            const genreObj = {
                name: 'genre1'
            };
            const genre = new Genre(genreObj);
            await genre.save();

            const response = await request(server).get(`/api/genres/${genre._id}`);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('_id', genre._id.toHexString());
            expect(response.body).toHaveProperty('name', genre.name);
        });

        it('should return the 404, if the genre id is invalid', async () => {            
            let response = await request(server).get(`/api/genres/1`);
            expect(response.status).toBe(404);            
        });
        it('should return the 404, if the no genre with the given id exist', async () => {
            const id = mongoose.Types.ObjectId();
            let response = await request(server).get(`/api/genres/${id}`);
            expect(response.status).toBe(404);
        });
    });

    describe('POST /', () => {

        let token;
        let name;

        beforeEach(() => {
            token = new User().generateAuthToken();
            name = 'genre1'
        });

        const exec = async () => {
            return await request(server)
                .post('/api/genres')
                .set('x-auth', token)
                .send({
                    name: name
                });
        };

        it('should return 401, if token is not passed', async () => {
            // Modifying token's value for sad path
            token = '';
            const response = await exec();
            expect(response.status).toBe(401);
        });


        it('should return 400, if genre is less then 3 characters', async () => {
            // Modifying name's value for sad path
            name = 'ab';
            const response = await exec();
            expect(response.status).toBe(400);
        });

        it('should return 400, if genre is less then 30 characters', async () => {
            // Modifying name's value for sad path
            name = new Array(32).join('a');
            const response = await exec();
            expect(response.status).toBe(400);
        });

        it('should save genre, if it is valid', async () => {
            // Here we don't modify any global variable's value to test its happy path
            await exec();
            const genre = await Genre.find({
                name: 'genre1'
            });
            expect(genre).not.toBeNull();
        });

        it('should return genre, if it is valid', async () => {
            // Here we don't modify any global variable's value to test its happy path
            const response = await exec();
            expect(response.body).toHaveProperty('_id');
            expect(response.body).toHaveProperty('name', 'genre1');
        });
    });

    describe('PUT /:id', () => {
        let token;
        let _id;
        let newName;

        beforeEach(async () => {
            let genre = new Genre({name: 'genre1'});
            await genre.save();

            token = new User().generateAuthToken();
            _id = genre._id;
            newName = 'updatedGenre1';
        });

        const exec = async () => {
            return await request(server)
                .put(`/api/genres/${_id}`)
                .set('x-auth', token)
                .send({
                    name: newName
                });
        }

        it('should return 401, if token is not passed', async () => {
            token = '';
            const response = await exec();
            expect(response.status).toBe(401);
        });
        it('should return 400, if genre name is less then 3 characters', async () => {
            newName = 'ab';
            const response = await exec();
            expect(response.status).toBe(400);
        });
        it('should return 400, if genre name is more than 30 characters', async () => {
            newName = new Array(32).join('a');
            const response = await exec();
            expect(response.status).toBe(400);
        });
        it('should update the document, if valid genre is sent with the valid id', async () => {
            await exec();
            const genre = await Genre.findById(_id);
            expect(genre.name).toBe(newName);
        });
        it('should return updated document, if valid genre is sent with the valid id', async () => {
            const response = await exec();
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('name', newName);
        });
        
    });

    describe('DELETE /:id', () => {
        let genreId;
        let token;
       
        beforeEach(async () => {
            token = new User({isAdmin: true}).generateAuthToken();
            let genre = new Genre({name: 'genre1'});
            await genre.save();
            genreId = genre._id;
        });

        const exec = () => {

            return request(server)
                    .delete(`/api/genres/${genreId}`)
                    .set('x-auth', token)
                    .send()
        }

        it('should return 401, if token is not passed', async () => {
            token = '';
            const response = await exec();
            expect(response.status).toBe(401);
        });

        it('should return 400, if invalid token', async () => {
            token = 'a';
            const response = await exec();
            expect(response.status).toBe(400);
        });

        it('should return 403, if not Admin', async () => {
            token = new User({isAdmin: false}).generateAuthToken();
            const response = await exec();
            expect(response.status).toBe(403);
        });

        it('should return 404, if the id not exist', async () => {
            genreId = mongoose.Types.ObjectId;
            const response = await exec();
            expect(response.status).toBe(404);
        });

        it('should delete genre, if the id is valid', async () => {
            await exec();
            const genre = await Genre.findById(genreId);
            expect(genre).toBeNull();
        });

        it('should return 200, if the id is valid', async () => {
            const response = await exec();
            expect(response.status).toBe(200);
        });

        it('should return deleted genre, if the id is valid', async () => {
            const response = await exec();
            expect(response.body).toHaveProperty('_id', genreId.toHexString());
            expect(response.body).toHaveProperty('name', 'genre1');
        });
    });
});

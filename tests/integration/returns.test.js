const {Rental} = require('../../models/rental');
const {User} = require('../../models/user');
const {Movie} = require('../../models/movie');

const mongoose = require('mongoose');
const moment = require('moment');
const request = require('supertest');

describe('/api/returns', () => {

    let server;
    let token;
    let customerId;
    let movieId;
    let rental;
    let movie;

    beforeEach(async () => {
        server = require('../../index');
        token = new User().generateAuthToken();

        customerId = mongoose.Types.ObjectId();
        movieId = mongoose.Types.ObjectId();

        movie = new Movie({
                _id: movieId,
                title: '12345',
                dailyRentalRate: 2, 
                genre: {name: '123'}, 
                numberInStock: 10
        });
        await movie.save();

        rental = new Rental({
            customer: {
                _id: customerId, 
                name: '12345', 
                phone: '12345'
            }, 
            movie: {
                    _id: movieId,
                    title: '12345',
                    dailyRentalRate: 2
            }
        });
        await rental.save();
    });

    afterEach(async () => {
        await server.close();    
        await Rental.remove({});    
        await Movie.remove({});
    });
    
    const exec = () => {
        return request(server)
                .post('/api/returns')
                .set('x-auth', token)
                .send({customerId, movieId});
    }

    it('should return 401 if client is not logged in', async () => {
        token = '';
        const response = await exec();
        expect(response.status).toBe(401);
    });

    it('should return 400 if customer id is not provided', async() => {
        customerId = '';
        const response = await exec();
        expect(response.status).toBe(400);
    });

    it('should return 400 if movie id is not provided', async () => {
        movieId = '';
        const response = await exec();
        expect(response.status).toBe(400);
    });

    it('should return 404 if no rental found for the customer/movie', async () => {
        // customerId = mongoose.Types.ObjectId();
        // movieId = mongoose.Types.ObjectId();
        await Rental.remove({});
        const response = await exec();
        expect(response.status).toBe(404);
    });

    it('should return 400 if rental already processed', async () => {
        await Rental.findByIdAndUpdate(rental._id, {
            dateReturned: new Date()
        });
        // rental.dateReturned = new Date();
        // await rental.save();
        const response = await exec();
        expect(response.status).toBe(400);
    });

    it('should return 200 if we have a valid request', async () => {
        const response = await exec();
        expect(response.status).toBe(200);
    });

    it('should set the returnDate if we have a valid request', async () => {
        const response = await exec();        
        rental = await Rental.findById(rental._id);

        // Too General
        // expect(rental.dateReturned).toBeDefined();

        // Best way to wrote test to check if the type of dateReturned is Date is to find a difference between the current date and dateReturned.  The difference should be less then 10,000 milli-sec
        const diff = new Date() - rental.dateReturned;
        expect(diff).toBeLessThan(10 * 1000);
    });

    it('should set the rentalFee if we have a valid request', async () => {
        rental.dateOut = moment().add(-7, "days").toDate();
        await rental.save();
        await exec();

        rental = await Rental.findById(rental._id);
        // dateOut * movie.dailyRentalRate        
        expect(rental.rentalFee).toBe(14); // dateOut is 7 days before the current Date and movie.dailyRentalRate = 2.  So, 7 * 2 = 14
    });

    it('should increase the movie stock if input is valid', async () => {        
        await exec();
        const updatedMovie = await Movie.findById(movieId);
        expect(updatedMovie.numberInStock).toBe(movie.numberInStock + 1);
    });

    it('should return the rental if input is valid', async () => {
        const response = await exec();
        const rentalInDb = await Rental.findById(rental._id);
        // expect(Object.keys(response.body)).toEqual(['_id'
        //     , 'customer', 'movie', 'dateOut', '__v', 'dateReturned', 'rentalFee']); // Here the order of an array matters
        expect(Object.keys(response.body)).toEqual(
            expect.arrayContaining(['movie', 'dateOut', 'customer', 'dateReturned', 'rentalFee', '_id'])); // Here the order of an array doesn't matters and also we didn't wants to check for all elements.  Here we leave __v, but still it works
    });
});
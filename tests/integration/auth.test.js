const request = require('supertest');
const {
    Genre
} = require('../../models/genre');
const {
    User
} = require('../../models/user');



describe('auth middleware', () => {

let server;
    beforeEach(() => {
        server = require('../../index');
    })
    afterEach(async () => {
        await Genre.remove({});
        await server.close();
    });

    let token;

    const exec = () => {
        return request(server)
            .post('/api/genres')
            .set('x-auth', token)
            .send({
                name: 'genre1'
            });
    }

    beforeEach(() => token = new User().generateAuthToken())

    it('should return 401 if no token is provided', async () => {
        token = '';
        const result = await exec();
        expect(result.status).toBe(401);
    });

    it('should return 401 if token is invalid', async () => {
        token = 'a';
        const result = await exec();
        expect(result.status).toBe(400);
    });

    it('should return 200 if token is valid', async () => {
        const result = await exec();
        expect(result.status).toBe(200);
    });
});
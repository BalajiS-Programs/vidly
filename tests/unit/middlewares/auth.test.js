const {User} = require('../../../models/user');
const auth = require('../../../middlewares/auth');
const mongoose = require('mongoose');

describe('auth middleware', () => {
    it('should populate req.user with the payload of a valid JWT', () => {
        const user = {
            _id: mongoose.Types.ObjectId().toHexString(),
            isAdmin: false
        }
        const req = {
            header: jest.fn().mockReturnValue(new User(user).generateAuthToken())
        };
        const res = {};
        const next = jest.fn();
        auth(req, res, next);

        expect(next).toHaveBeenCalled();
        // expect(req.user).toHaveProperty('_id', user._id.toHexString());
        expect(req.user).toMatchObject(user);
    });
});
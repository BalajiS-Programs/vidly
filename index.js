const winston = require('winston');
const express = require('express');

const app = express();

require('./startup/logging')(); // It should loaded first, to log if any error occurs on the following modules
require('./startup/config')();
require('./startup/routes')(app);
require('./startup/db')();
require('./startup/validation')();
require('./startup/prod')(app);

// // throw new Error('Uncaught Exception');
// const p = Promise.reject(new Error('Unhandled Rejection'));
// p.then(() => console.log('Done'));
//     // .catch((ex) => console.log(ex.message));

const port = process.env.PORT || 8888;
const server = app.listen(port, () => {
    winston.info(`Server started on port ${port}`);
});

module.exports = server;
